import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'

const LoginScreen = (props: any) => {
    const {navigation} = props
  return (
    <View>
      <Text>LoginScreen</Text>
      <TouchableOpacity onPress={() => navigation.navigate('NAVIGATION_HOME')}>
        <Text>Login</Text>
      </TouchableOpacity>
    </View>
  )
}

export default LoginScreen

const styles = StyleSheet.create({})