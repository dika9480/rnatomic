import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const HomeScreen = () => {
  return (
    <View style={styles.viewContainer}>
      <Text>Halo Selamat Datang</Text>
    </View>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})