import React from 'react';
import { createStackNavigator, NavigationContainer, TransitionPresets } from '../libraries';
import { HomeScreen } from '../screens';
import { NAVIGATION_HOME, NAVIGATION_LOGIN } from '../constans';
import LoginScreen from '../screens/Login';

const Stack = createStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName='NAVIGATION_HOME'
        screenOptions={({}) => ({
          ...TransitionPresets.SlideFromRightIOS,
          headerShown: false,
          gestureEnabled: true,
          cardOverlayEnabled: true,
        })}>
        <Stack.Screen name={NAVIGATION_HOME} component={HomeScreen} />
        <Stack.Screen name={NAVIGATION_LOGIN} component={LoginScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;