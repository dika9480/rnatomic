import ApiRequest from './config';
import {Config} from '../../libraries';

const API = {
  /**
   * ====================================================
   * AUTH
   * ====================================================
   */
  reqLogin: ApiRequest.post(''),
};

export default API;